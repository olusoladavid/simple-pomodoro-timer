import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import angularLoadingBar from 'angular-loading-bar';
import notifySound from '../assets/audio/notify.mp3';

// bootstrap
import { RootComponent } from './root.component';
// modules
import { CommonModule } from './common/common.module';
import './root.component.scss';

const MODULE_NAME = 'root';
const MODULE_IMPORTS = [
  uiRouter,
  angularLoadingBar,
  CommonModule
];
const pomodoroSessions = [
  {
    stage: 'pomodoro',
    minutes: 0,
    seconds: 10
  },
  {
    stage: 'shortBreak',
    minutes: 5,
    seconds: 0
  },
  {
    stage: 'longBreak',
    minutes: 10,
    seconds: 0
  }
];
console.log(notifySound);

angular
  .module(MODULE_NAME, MODULE_IMPORTS)
  .component(RootComponent.selector, RootComponent)
  .value('PomodoroSessions', pomodoroSessions)
  .value('NotificationSound', notifySound)
  .run(($transitions, cfpLoadingBar) => {
    'ngInject';

    $transitions.onStart({}, cfpLoadingBar.start);
    $transitions.onSuccess({}, cfpLoadingBar.complete);
  })
  .name;
