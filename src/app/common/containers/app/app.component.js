export const AppComponent = {
  selector: 'app',
  template: `
    <div class="app">
      <app-nav></app-nav>
      <div class="row">
        <div class="tab-layout">
          <div class="{{'tab ' + ($ctrl.currentTab === 'pomodoro' ? 'active': '')}}">
            Pomodoro
          </div>
          <div class="{{'tab ' + ($ctrl.currentTab === 'shortBreak' ? 'active': '')}}">
            Short Break
          </div>
          <div class="{{'tab ' + ($ctrl.currentTab === 'longBreak' ? 'active': '')}}">
            Long Break
          </div>
        </div>
      </div>
      <div class="row">
        <div class="timer" ng-bind="$ctrl.timer.minutes + ':' + $ctrl.timer.seconds">
        </div>
      </div>
      <div class="row">
        <div class="buttons">
          <button class="{{'button ' + (!$ctrl.timer.isRunning ? 'active': '')}}" ng-click="$ctrl.timer.start()">Start</button>
          <button class="{{'button ' + ($ctrl.timer.isRunning ? 'active': '')}}" ng-click="$ctrl.timer.stop()">Stop</button>
          <button class="button" ng-click="$ctrl.timer.reset()">Reset</button>
        </div>
      </div>
      <ui-view></ui-view>
    </div>
  `,
  controller: class AppComponent {
    constructor($interval, PomodoroSessions, NotificationSound) {
      'ngInject';

      this.pomodoroSessions = PomodoroSessions;
      this.notifySound = NotificationSound;
      this.sessionsCount = 0;
      this.activeSession = this.pomodoroSessions[0];
      this.currentTab = this.activeSession.stage;
      const self = this;
      this.timer = {
        isRunning: false,
        ticker: null,
        _minutes: self.activeSession.minutes,
        _seconds: self.activeSession.seconds,
        get minutes() {
          return this.toDigitString(this._minutes);
        },
        set minutes(value) {
          if (value < 0) {
            this.stop();
            self.notifyUser();
            self.switchSession();
          } else {
            this._minutes = value;
          }
        },
        get seconds() {
          return this.toDigitString(this._seconds);
        },
        set seconds(value) {
          if (value < 0) {
            this.minutes = parseInt(this.minutes) - 1;
            this._seconds = this.isRunning ? 59 : 0;
          } else {
            this._seconds = value;
          }
        },
        toDigitString(val) {
          return val < 10 ? ('0' + val) : ('' + val);
        },
        start() {
          this.isRunning = true;
          this.ticker = $interval(() => {
            this.seconds = parseInt(this.seconds) - 1;
          }, 1000);
        },
        stop() {
          this.isRunning = false;
          $interval.cancel(this.ticker);
        },
        reset() {
          this.stop();
          this.minutes = self.activeSession.minutes;
          this.seconds = self.activeSession.seconds;
        }
      };
    }

    switchSession() {
      this.sessionsCount += 1;
      this.activeSession = this.pomodoroSessions[this.sessionsCount % 3];
      this.currentTab = this.activeSession.stage;
      this.timer.minutes = this.activeSession.minutes;
      this.timer.seconds = this.activeSession.seconds;
    }

    notifyUser() {
      const audio = new Audio(this.notifySound);
      audio.play();
    }
  }
};
