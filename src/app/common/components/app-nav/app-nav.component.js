export const AppNavComponent = {
  selector: 'appNav',
  template: `
    <header>
      <div class="header__fixed">
        <div class="header__wrapper">
          <h1 class="app-title">Pomodoro Timer</h1>
        </div>
      </div>
    </header>
  `,
};
